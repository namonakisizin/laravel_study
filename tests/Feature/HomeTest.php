<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testStatusCode()
    {
        $response = $this->get('/home');  /*アクセス結果が$responseに格納されること */

        $response->assertStatus(200); /* HTTPステータスコードが200であること */
    }
    public function testBody()
    {
        $responce = $this->get('/home');
        $responce->assertSeeText("こんにちは!");
    }
}
